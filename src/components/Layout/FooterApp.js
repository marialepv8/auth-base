import './Footer.css';
import { Layout, Row, Col } from 'antd';
import {
  WhatsAppOutlined,
  TwitterOutlined,
  FacebookOutlined
}
from '@ant-design/icons';

const { Footer } = Layout;

const FooterApp = ( ) => {
  return (
    <Layout>
      <Footer className="footer">
        <Row gutter={16}>
          <Col>
            <a href="https://web.whatsapp.com/" target="_blank" rel="noreferrer" >
              <WhatsAppOutlined />
            </a>
          </Col>
          <Col>
            <a href="https://twitter.com/?lang=es" target="_blank" rel="noreferrer" >
              <TwitterOutlined />
            </a>
          </Col>
          <Col>
            <a href="https://www.facebook.com/" target="_blank" rel="noreferrer" >
              <FacebookOutlined />
            </a>
          </Col>
        </Row>
      </Footer>
    </Layout>
  );
}

export default FooterApp;