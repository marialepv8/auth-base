import './LayoutApp.css';
import { Layout, Card } from 'antd';
import Header from './HeaderApp';
import Footer from './FooterApp';

const { Content } = Layout;

const LayoutApp = ( props ) => {
  const { token, setToken } = props;

  return (
    <Layout>
      <Header token={token} setToken={setToken} />
      <Content className="content">
        <Card className="content-card">
          {props.children}
        </Card>
      </Content>
      <Footer />
    </Layout>
  );
}

export default LayoutApp;