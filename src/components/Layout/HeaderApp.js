import './Header.css';
import { Layout, Row, Col, Tooltip } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';

const { Header } = Layout;

const HeaderApp = ( props ) => {
  const { token, setToken } = props;
  const history = useHistory();

  const onCLickLogOut = async () => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_API}/logout/`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      }
    })
    const data = await res.json();
    setToken(data);
    localStorage.clear();
    history.push('/');
  }

  return (
    <Layout>
      <Header className="header">
        <Row gutter={4}>
          <Col span={23}>
            <div className="logo"></div>
          </Col>
          <Col span={1} >
            {
              token &&
              <Tooltip placement="topLeft" title="Log Out" >
                <LogoutOutlined onClick={onCLickLogOut} />
              </Tooltip>
            }
          </Col>
        </Row>
      </Header>
    </Layout>
  );
}

export default HeaderApp;