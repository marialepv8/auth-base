import './App.css';
import React from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';
import LayoutApp from '../Layout/LayoutApp';
import Login from '../Login/Login';
import Register from '../Register/Register';
import Dashboard from '../Dashboard/Dashboard';
import useToken from './useToken';

const App = ( ) => {
  const { token, setToken } = useToken();
  const history = useHistory();

  if(!token) {
    history.push('/');
    localStorage.clear();
  }

  return (
    <LayoutApp token={token} setToken={setToken} >
      <Switch>
        {
          !token ?
          (
            <React.Fragment>
              <Route exact path="/" component={Login}><Login setToken={setToken} /></Route>
              <Route path="/register" component={Register}><Register /></Route>
            </React.Fragment>
          )
          :
          (
            <React.Fragment>
              <Route path="/dashboard" component={Dashboard}><Dashboard token={token} /></Route>
            </React.Fragment>
          )
        }
      </Switch>
    </LayoutApp>
  );
}

export default App;
