import './App.css';
import { Layout, Row, Col, Card } from 'antd';
import { 
  WhatsAppOutlined, 
  TwitterOutlined, 
  FacebookOutlined,
  LogoutOutlined 
} from '@ant-design/icons';
import { 
  Route, 
  useLocation 
} from 'react-router-dom';
import Login from '../Login/Login';
import Register from '../Register/Register';
import Dashboard from '../Dashboard/Dashboard';
import useToken from './useToken';

const { Header, Footer, Content } = Layout;

const App = () => {
  const { token, setToken } = useToken();
  /* const history = useHistory(); */
  const location = useLocation();

 /*  const onCLickLogOut = async () => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_API}/logout/`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      }
    })
    const data = await res.json();
    setToken(data);
    localStorage.clear();
    
  }

  if (token) {
    history.replace("/dashboard");
    
  } */

  return (
    <Layout>
      <Header className="header">
        <div className="logo"></div>
        {
          token &&
          <LogoutOutlined onClick={onCLickLogOut}/>
        }
      </Header>
      <Content className="content">
        <Card className="content-card">
          {
            !token ?
            <Row>
              <Col span={12} className={location.pathname === '/register' ? "color-backgroud" : "" } >
                {
                  location.pathname === '/' && 
                  <Route path="/" component={Login}><Login setToken={setToken} /></Route>
                }
              </Col>
              <Col span={12} className={location.pathname === '/' ? "color-backgroud" : "" } >
                {
                  location.pathname === '/register' && 
                  <Route path="/register" component={ Register }><Register /></Route>
                }
              </Col>
            </Row>
            :
            <div>
              <Route path="/dashboard" component={Dashboard}><Dashboard /></Route>
            </div>
          }
        </Card>
      </Content>
      <Footer className="footer">
        <Row gutter={ 16 }>
          <Col>
            <a href="https://web.whatsapp.com/" target="_blank" rel="noreferrer" >
              <WhatsAppOutlined />
            </a>
          </Col>
          <Col>
            <a href="https://twitter.com/?lang=es" target="_blank" rel="noreferrer" >
              <TwitterOutlined />
            </a>
          </Col>
          <Col>
            <a href="https://www.facebook.com/" target="_blank" rel="noreferrer" >
              <FacebookOutlined />
            </a>
          </Col>
        </Row>
      </Footer>
    </Layout>
  );
}

export default App;
