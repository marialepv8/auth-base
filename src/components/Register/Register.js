import './Register.css';
import { Form, Input, Button, notification } from 'antd';
import { Link, useHistory } from 'react-router-dom';

const layout = {
  labelCol: { span: 12 },
  wrapperCol: { span: 12 }
};

const Register = () => {
  const history = useHistory();
  const initialValues = {
    name: '',
    email: '',
    username: '',
    password: ''
  };

  const onFinish = async ( values ) => {
    delete values.confirmPassword;

    const res = await fetch(`${process.env.REACT_APP_BACKEND_LOCAL}/users`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(values)
    })

    const data = await res.json();
    
    if (data) {
      notification.success({
        message: 'Notification Registration',
        description: 'Registration completed successfuly.',
        placement: 'bottomRight'
      });

      history.replace('/');
    } else {
      notification.warning({
        message: 'Notification Registration',
        description: 'Something is wrong with the registration',
        placement: 'bottomRight'
      });
    }
  };

  return (
    <div>
      <h1 className="text-align-center animate__animated animate__backInUp">Register</h1>

      <Form 
        { ...layout }
        name="formSignUpRegister"
        initialValues={ initialValues }
        onFinish={ onFinish }
        className="formSignUp animate__animated animate__backInRight animate__delay-1s"
        scrollToFirstError
      >
        <Form.Item name="name" label="Name" rules={[{ required: true, message: "Name is required" }]}>
          <Input />
        </Form.Item>
        <Form.Item 
          name="email" 
          label="E-mail" 
          rules={[
            { required: true,
              message: "E-mail is required"
            },
            {
              type: "email",
              message: "E-mail is invalid"
            }
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item name="username" label="Username" rules={[{ required: true, message: "Username is required" }]}>
          <Input />
        </Form.Item>
        <Form.Item name="password" label="Password" rules={[{ required: true, message: "Password is required" }]}>
          <Input.Password />
        </Form.Item>
        <Form.Item 
          name="confirmPassword" 
          label="Confirm Password" 
          dependencies={['password']}
          rules={[
            { required: true,
              message: "Confirmation is required" 
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('The two passwords that you entered do not match!'));
              }
            })
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 12 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
      <div className="text-align-center">
        <span>If you already have an account </span> <Link to='/'>Log In</Link>
      </div>
    </div>
  );
}

export default Register;