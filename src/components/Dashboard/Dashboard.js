import { Card, Row, Col } from 'antd';
import { useState, useEffect } from 'react';

const Dashboard = ( props ) => {
  const { token } = props;
  const [ properties, setProperties ] = useState([]);
  
  useEffect(() => {
    getProperties();
  }, []);

  const getProperties = async () => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_API}/inventory/property/`, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    });

    const data = await res.json();
    setProperties(data);
  }
  
  const updateProperties = async ( id ) => {
    await fetch(`${process.env.REACT_APP_BACKEND_API}/inventory/property/${id}/`, {
      method: 'PATCH',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({name: 'anchoa'})
    });
    getProperties();
  }

  const removeProperty = async ( id ) => {
    await fetch(`${process.env.REACT_APP_BACKEND_API}/inventory/property/${id}/`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    getProperties();
  }

  const addProperty = async () => {
    await fetch(`${process.env.REACT_APP_BACKEND_API}/inventory/property/`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({name:'prueba'})
    })
    getProperties();
  }

  return (
    <div>
      <h1>Dashboard</h1>
      <div onClick={() => addProperty()}>ADD</div>

      <Row gutter={[16, 16]} wrap>
        { 
          properties.map((property) => (
            <Col key={property.id} span={8}>
              <Card title={property.name} onClick={() => updateProperties(property.id)}>
                <div onClick={() => removeProperty(property.id)}>REMOVE</div>
              </Card>
            </Col>
          ))
        }
      </Row>
      
    </div>
  )
}

export default Dashboard;